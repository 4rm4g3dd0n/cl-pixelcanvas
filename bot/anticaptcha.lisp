;;;; An interface for anti-captcha.com.

(in-package :pixelcanvas-bot)

(defvar solve-queue nil)
(defvar *service-queue* nil)
(defvar *api-key* "GetYourOwn")
(defvar *captcha-key* "6Le6siQUAAAAAEe7Y0HGLtcvMHN2f8d65x4sJo2Y")
(defvar *service-lock* (bt:make-lock "*service-queue* lock"))

(defun post-solve (proxy)
  (let ((response-id
         (cdr
          (assoc :task-id
                 (cl-json:decode-json-from-string
                  (map 'string #'code-char
                       (drakma:http-request "https://api.anti-captcha.com/createTask"
                                            :method :post
                                            :connection-timeout 20
                                            :content (cl-json:encode-json-to-string
                                                      `((:client-key . ,*api-key*)
                                                        (:task . (
                                                                  (:type . "NoCaptchaTaskProxyless")
                                                                  ("websiteURL" . "http://pixelcanvas.io")
                                                                  (:website-key . ,*captcha-key*))))))))))))
    (when response-id
      (bt:with-lock-held (*service-lock*)
        (push (cons proxy response-id) *service-queue*)))))

(defun reset-pixelcanvas-proxy (proxy &optional token)
  (setf (pixelcanvas-client-err proxy) nil)
  (setf (pixelcanvas-client-fingerprint-ok proxy) t)
  (setf (pixelcanvas-client-captcha-token proxy) token)
  (setf (pixelcanvas-client-wait-time proxy) (get-internal-real-time)))

(defun get-status (id)
  (let ((response
         (cl-json:decode-json-from-string
          (map 'string #'code-char
               (drakma:http-request "https://api.anti-captcha.com/getTaskResult"
                                    :method :post
                                    :connection-timeout 20
                                    :content (cl-json:encode-json-to-string
                                              `((:client-key . ,*api-key*)
                                                (:task-id . ,id))))))))
    (when (string= (cdr
                    (assoc :status response))
                   "ready")
      (cdr
       (assoc :g-recaptcha-response
              (cdr
               (assoc :solution response)))))))

(defun check-response-worker ()
  (loop for item = (bt:with-lock-held (*service-lock*)
                     (pop *service-queue*))
     for (proxy . id) = item
     when (and item proxy)
     do (unless (null (pixelcanvas-client-err proxy))
          (let ((response
                 (ignore-errors
                   (get-status id))))
            (if response
                (reset-pixelcanvas-proxy proxy response)
                (bt:with-lock-held (*service-lock*)
                  (push item *service-queue*)))))
     do (sleep 0.5)))
            
(defun post-solve-worker ()
  (loop
       (loop for proxy = (bt:with-lock-held (*buffer-lock*)
                           (pop solve-queue))
          unless (or (null proxy) (sleep 1))
          unless (member proxy *service-queue* :key #'car)
          unless (ignore-errors
                   (post-solve proxy))
          do (bt:with-lock-held (*buffer-lock*)
               (setf (pixelcanvas-client-err proxy)
                     "Waiting for captcha")
               (push proxy solve-queue))
          do (sleep 0.5))))

(when *api-key*
  (defvar solver (bt:make-thread #'post-solve-worker :name "Solving worker"))
  (defvar responder (bt:make-thread #'check-response-worker :name "Response checking worker")))
