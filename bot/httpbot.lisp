(ql:quickload '(:hunchentoot :cl-who :exit-hooks :bordeaux-threads :split-sequence :flexi-streams :drakma :pixelcanvas))
(in-package :pixelcanvas-bot)

(defvar *pixel-buffer* nil)
(defvar *buffer-lock* (bt:make-lock "Pixel buffer lock"))
(defvar *proxies* nil)
(defvar *pages* '(("/" "overview")
		  ("/add-proxy.human" "add proxy")
                  ("/add-image.human" "add image")
                  ("/view-images" "view images")
                  ("/telemetry" "stats")
                  ("/logout" "log out")))

(setf (html-mode) :html5)

(define-easy-handler (http-proxy-list :uri "/") ()
  (with-authentication
    (basic-page "Proxies"
      (navbar)
      (:h1 "Bot overview")
      (:p (str (format nil "~d image~:p in stack, ~d pixel~:p in stack"
                       (length *pixel-buffer*)
                       (+
                        (length *stray-pixels*)
                        (loop for image in *pixel-buffer*
                           summing (length (pixelcanvas-image-pixels image)))))))
      (:p (str (format nil "~d token~:p queued"
                       (queue-count *captcha-queue*))))
      (:p (str (format nil "~d prox~@:p, ~d prox~:@p require fingerprints, ~d prox~:@p ready to go"
                       (length *proxies*)
                       (loop for proxy in *proxies*
                          counting (and (not (pixelcanvas-client-fingerprint-ok proxy))
                                        (not (pixelcanvas-client-err proxy))))
                       (loop for proxy in *proxies*
                          counting (and (pixelcanvas-client-fingerprint-ok proxy)
                                        (null
                                         (pixelcanvas-client-err proxy)))))))
      (:table
       (:thead
        (:tr
         (:th "Proxy")
         (:th "Fingerprint")
         (:th "Status")
         (:th "Actions")))
       (:tbody
        (loop for proxy in *proxies* do
             (htm (:tr :class
                       (cond
                         ((not (pixelcanvas-client-fingerprint-ok proxy))
                          "proxy-fingerprint-error")
                         ((not (pixelcanvas-readyp proxy))
                          "proxy-waiting")
                         (t "proxy-ok"))
                       (:td :class "proxy" :data-label "Proxy"
                            (esc (format nil "~{~a~^:~}" (or (pixelcanvas-client-proxy proxy)
                                                             '("this machine")))))
                       (:td :class "fingerprint" :data-label "Fingerprint"
                            (esc (pixelcanvas-client-fingerprint proxy)))
                       (:td :class "status" :data-label "Status"
                            (cond
                              ((pixelcanvas-client-err proxy)
                               (esc (pixelcanvas-client-err proxy)))
                              ((not (pixelcanvas-client-fingerprint-ok proxy))
                               (str "Bad fingerprint!"))
                              ((not (pixelcanvas-readyp proxy))
                               (str
                                (format nil "Waiting ~,2f second~:p"
                                        (/ (- (pixelcanvas-client-wait-time proxy)
                                              (get-internal-real-time))
                                           internal-time-units-per-second))))
                              (t (str "All good"))))
                       (:td :class "reset" :data-label "Actions"
                            (when (or (not (pixelcanvas-client-fingerprint-ok proxy))
                                      (pixelcanvas-client-err proxy))
                              (htm
                               (:a :class "button"
                                   :href (format nil "/proxy-reset?ip=~a&port=~a"
                                                 (first
                                                  (pixelcanvas-client-proxy proxy))
                                                 (second
                                                  (pixelcanvas-client-proxy proxy)))
                                   "Reset")))
                            (:a :class "button"
                                :href (format nil "/proxy-remove?ip=~a&port=~a"
                                              (first
                                               (pixelcanvas-client-proxy proxy))
                                              (second
                                               (pixelcanvas-client-proxy proxy)))
                                "Remove"))))))))))

(define-easy-handler (http-add-proxy-form :uri "/add-proxy.human") ()
  (basic-page "Add proxy"
    (navbar)
    (:h1 "Add a proxy")
    (:form :method "POST" :action "/add-proxy"
           (:label :for "proxy" "Proxy name:")
           (:input :name "proxy")
           (:label :for "port" "Port:")
           (:input :name "port")
           (:br)
           (:label :for "fingerprint" "Fingerprint:")
           (:input :name "fingerprint")
           (:br)
           (:button :type "submit" "Send"))))

(defun random-fingerprint ()
  (format nil "~(~32,'0x~)"
          (random (expt 2 128))))

(defun add-proxy (proxy port fingerprint)
  (let ((proxy-description
         (unless (string= proxy "localhost")
           (list proxy (parse-integer port)))))
    (setf *proxies*
          (delete proxy-description *proxies* :test #'equalp :key #'pixelcanvas-client-proxy))
    (push (make-pixelcanvas-client
           :proxy proxy-description
           :fingerprint fingerprint)
          *proxies*)))
  
(define-easy-handler (http-add-proxy :uri "/add-proxy") (proxy port fingerprint)
  (handler-case
      (if (and proxy port)
          (progn
            (add-proxy proxy port (if (zerop (length fingerprint))
                                      (random-fingerprint)
                                      fingerprint))
            (redirect-page "added proxy" "/"
              (esc
               (format nil "Added ~a ↦ ~a. Thanks." proxy fingerprint))))
          "Both the proxy and fingerprint are required.")
    (condition (c)
      (format nil "Error: ~a" c))))

(define-easy-handler (http-add-proxies :uri "/add-proxies") (import-text seperator)
  (with-authentication
    (let* ((lines (split-sequence #\Newline (string-trim " 
" import-text)))
           (bits  (loop for line in lines
                     collect (split-sequence (coerce seperator 'character) line)))
           (proxy-specs
            (loop for bit in bits
               collect (destructuring-bind (ip port &optional (fingerprint (random-fingerprint))) bit
                         (list ip port fingerprint)))))
      (mapcar (lambda (bit)
                (apply #'add-proxy bit))
              proxy-specs)
      (basic-page "import status"
        (:p "Imported the following proxies:"
            (:table
             (:thead
              (:tr
               (:th "IP")
               (:th "Port")
               (:th "Fingerprint")))
             (:tbody
              (loop for (ip port fingerprint) in proxy-specs
                 do (htm
                     (:tr
                      (:td :data-label "IP" (str ip))
                      (:td :data-label "Port" (str port))
                      (:td :data-label "Fingerprint" (str fingerprint))))))))
        (:p
         (:a :href "/im-very-careful" "Click here to go back to the OP console."))))))

(define-easy-handler (http-view-images :uri "/view-images") ()
  (with-authentication
    (basic-page "Image stack"
      (navbar)
      (:h1 "Images on stack")
      (:table
       (:thead
        (:tr
         (:th "Image URL")
         (:th "Offset")
         (:th "Pixels remaining")
         (:th "Actions")
         (:th "Flags")))
       (:tbody
        (loop for image in *pixel-buffer* do
             (htm (:tr
                   (:td :class "image-url" :data-label "URL"
                        (:a :href (pixelcanvas-image-name image)
                            (str (pixelcanvas-image-name image))))
                   (:td :class "image-offset" :data-label "Offset"
                        (str (format nil "(~d, ~d)"
                                     (pixelcanvas-image-offset-x image)
                                     (pixelcanvas-image-offset-y image))))
                   (:td :class "image-remaining" :data-label "Pixels"
                        (str (length (pixelcanvas-image-pixels image))))
                   (:td :class "image-actions" :data-label "Actions"
                        (:a :href (format nil "/remove-image?url=~a"
                                          (escape-string (pixelcanvas-image-name image)))
                            :class "button"
                            "Remove"))
                   (:td :class "image-flags" :data-label "Flags"
                        (str (format nil *english-list* (pixelcanvas-image-flags image)))))))))
      (:p
       (esc (format nil "This table does not include ~d stray pixel~:p that must be re-plotted due to proxy errors."
                    (length *stray-pixels*)))))))

(define-easy-handler (http-remove-proxy :uri "/proxy-remove") (ip port)
  (with-authentication
    (if (and ip port)
        (progn
          (setf *proxies*
                (delete (unless (string= ip "NIL")
                          (list ip
                                (parse-integer port)))
                      *proxies* :test #'equalp :key #'pixelcanvas-client-proxy))
          (redirect-page "removed proxy" "/"
            (str
             (format nil "Removed the proxy ~a. That's a shame." ip))))
        "I need a proxy to remove.")))

(define-easy-handler (http-add-image :uri "/add-image") (x y image no-rescan)
  (with-authentication
    (handler-case
        (let ((x (parse-integer x))
              (y (parse-integer y)))
          (bt:with-lock-held (*buffer-lock*)
            (push (make-pixelcanvas-image
                   :pixels (filter-wrong
                            (pixelcanvas::read-png-stream
                             (flexi-streams:make-in-memory-input-stream
                              (drakma:http-request image)))
                            :offset-x x
                            :offset-y y)
                   :name image
                   :offset-x x
                   :offset-y y
                   :flags (when no-rescan
                            (list :no-rescan)))
                  *pixel-buffer*)))
      (condition (e) (format nil "An error occured: ~a" e))
      (:no-error (ret)
        (declare (ignore ret))
        (redirect-page "added image" "/"
          "Added the image.")))))

(define-easy-handler (http-remove-image :uri "/remove-image") (url)
  (with-authentication
    (bt:with-lock-held (*buffer-lock*)
      (setf *pixel-buffer*
            (delete url *pixel-buffer* :key #'pixelcanvas-image-name :test #'string=)))
    (redirect-page "removed image" "/"
      "Removed the image.")))

(define-easy-handler (http-add-image-form :uri "/add-image.human") ()
  (with-authentication
    (basic-page "Add image"      
      (navbar)
      (:h1 "Add an image")
      (:form :method "POST" :action "/add-image"
             (:label :for "x" "X offset:")
             (:input :name "x")
             (:br)
             (:label :for "y" "Y offset:")
             (:input :name "y")
             (:br)
             (:label :for "image" "Image URL:")
             (:input :name "image")
             (:br)
             (:input :type "checkbox"
                     :name "no-rescan"
                     :value "enabled")
             (:label :for "no-rescan" "Disable canvas rescans")
             (:hr)
             (:button :type "submit" "Send")))))

(defun find-proxy-by-name (list name port)
  (loop
     with ip-port = (unless (string= name "NIL")
                      (list name (parse-integer port)))
     for proxy in list
     when (equal (pixelcanvas-client-proxy proxy) ip-port)
     do (return proxy)))

(define-easy-handler (http-reset-proxy :uri "/proxy-reset") (ip port)
  (with-authentication
    (let ((proxy (find-proxy-by-name *proxies* ip port)))
      (if proxy
          (progn
            (setf (pixelcanvas-client-err proxy) nil)
            (setf (pixelcanvas-client-fingerprint-ok proxy) t)
            (setf (pixelcanvas-client-wait-time proxy) (get-internal-real-time))
            (redirect-page "reset proxy" "/"
              (str (format nil "The figurative breaker on ~a has been reset." ip))))
          "There aren't any proxies by that name."))))

(define-easy-handler (http-telemetry :uri "/telemetry") ()
  (with-authentication
    (basic-page "Telemetry"
      (navbar)
      (:h1 "Telemetry information")
      (:h2 "Threads")
      (:ul
       (loop for thread in (bt:all-threads)
          do (htm (:li
                   (:code
                    (esc (format nil "~a" thread)))))))
      (:h2 "Workers")
      (:p
       (:b "Flags:")
       (str (format nil *english-list* *worker-flags*)))
      (:h2 "Recent pixels")
      (:p
       "Showing up to 1000 pixels. You can"
       (:a :href "/telemetry.csv"
           "download")
       (str (format nil "all ~d as a CSV too." (length *telemetry-stack*))))
      (:table
       (:thead
        (:tr
         (:th "Coordinates")
         (:th "Colour")
         (:th "Proxy")
         (:th "Response code")
         (:th "Token")))
       (:tbody
        (loop
           for i upto 1000
           for (x y colour proxy response token) in *telemetry-stack*
           do (htm
               (:tr
                (:td :data-label "Coordinates"
                     (str (format nil "(~d, ~d)" x y)))
                (:td :data-label "Colour"
                     (str (format nil "~@(~a~) (~d)"
                                  (second (pixelcanvas::colour-by-number colour))
                                  colour)))
                (:td :data-label "Proxy"
                     (esc (if (null (pixelcanvas-client-proxy proxy))
                              "No proxy"
                              (destructuring-bind (host port)
                                  (pixelcanvas-client-proxy proxy)
                                (format nil "~a:~d"
                                        host port)))))
                (:td :data-label "Response"
                     (str (format nil "~d" response)))
                (:td :data-label "Token"
                     (str (if token
                              (format nil "~a..." (subseq token 0 20))
                              "none")))))))))))

(define-easy-handler (http-telemetry-csv :uri "/telemetry.csv") ()
  (with-authentication
    (setf (content-type*) "text/csv")
    (format nil "X,Y,Colour-Number,Colour-Name,Proxy,Response-Code,Token~%~{~a~%~}"
            (loop for (x y colour proxy response) in *telemetry-stack*
               collect (format nil "~d,~d,~d,~a,~a,~d,~a"
                               x y colour
                               (second (pixelcanvas::colour-by-number colour))
                               (if (null (pixelcanvas-client-proxy proxy))
                                     "local"
                                     (destructuring-bind (host port)
                                         (pixelcanvas-client-proxy proxy)
                                       (format nil "~a:~d"
                                               host port)))
                               response
                               (or (pixelcanvas-client-captcha-token proxy)
                                   "none"))))))

(define-easy-handler (http-seriously-op-shit :uri "/im-very-careful") ()
  (with-authentication
    (basic-page "Seriously OP shit you shouldn't be able to use tbh"
      (navbar)
      (:h1 "Seriously OP shit you shouldn't be able to use tbh")
      (warning-card "i'm warning you fam"
        (:p "i'll regret giving this to you plebs actually"))
      (:div :class "row"
            (:div :class "col-md-6"
                  (:form
                   (:h2 "Reset all proxies")
                   (:p
                    "You really don't need to press this button unless cl-pixelcanvas hasn't been notified of something very odd. Don't use this if:"
                    (:ul
                     (:li "none of the captchas have fingerprints and it's only been half an hour")
                     (:li "you're okay with not getting help when everything breaks and somehow the bot goes even slower"))
                    (:a :href "/reset-all-proxies"
                        :class "button"
                        "Reset"
                        (:em "every")
                        "proxy, regardless of state")
                    (:br)
                    (:small "I realise I'm basically giving a toddler who's only used to play sword fighting something more dangerous than the US's nuclear arsenal with this button, let alone the rest of the bot. You can handle the responsibility of it, right?")
                    (when (zerop (random 42))
                      (htm
                       (:small
                        (:b "Edit:")
                        "An unconcerned server operator called /dev/ponies, who we sometimes call \"Devie\", said it's not that dangerous. Go prove her wrong."))))))
            (:div :class "col-md-6"
                  (:form :method :post
                         :action "/add-proxies"
                         (:h2 "Import proxy list")
                         (:p "Each proxy should be on a seperate line, with a seperator in between IP, port and an optional fingerprint.")
                         (:p "For example,"
                             (:code "123.45.67.89:8080:cafebabe")
                             "and"
                             (:code "135.79.246.80:3182")
                             "are both valid. The seperator can only be one character, but I'm not sure why it'd need to be longer.")
                         (:label :for "seperator"
                                 "Seperator:")
                         (:input :size 1
                                 :maxlength 1
                                 :value ":"
                                 :name "seperator")
                         (:br)
                         (:textarea :name "import-text")
                         (:br)
                         (:button :type :submit
                                  "Add proxies")))
            (:div :class "col-md-6"
                  (:form :method :post
                         :action "/set-flags"
                         (:h2 "Set worker flags")
                         (:input :type "checkbox"
                                 :name "must-have-tokens"
                                 :checked (member :must-have-tokens *worker-flags*))
                         (:label :for "must-have-tokens"
                                 (:code "Must-Have-Tokens")
                                 "(accumulates ready proxies and waits)")
                         (:br)
                         (:button :type :submit
                                  "Update")))))))

(define-easy-handler (http-reset-all-proxies :uri "/reset-all-proxies") (absolutely-sure)
  (with-authentication
    (if absolutely-sure
        (let ((resets (length
                       (bt:with-lock-held (*buffer-lock*)
                         (mapcar #'reset-pixelcanvas-proxy *proxies*)))))
          (redirect-page "reset all proxies"
              "/im-very-careful"
            (:p
             (esc (format nil "resetted ~d prox~@:p!" resets)))))
        (slow-redirect-page "are you absolutely sure?"
            "/im-very-careful"
          (:p "okay i don't trust any of you with this so just to be sure, press this button please:"
              (:a :href "/reset-all-proxies?absolutely-sure=maybe"
                  :class "button"
                  "reset!"))
          (:p "if not, just sit tight or click this button to get out:"
              (:a :href "/im-very-careful"
                  :class "button"
                  "get out!"))
          (:p "if you don't know how you got here, here's an easier bot to wrangle:"
            (:a :href "http://www.calormen.com/jslogo/"
                :class "button"
                "yay for HTML5"))))))

(define-easy-handler (http-set-flags :uri "/set-flags") (must-have-tokens)
  (redirect-page "set flags"
      "/im-very-careful"
    (str
     (format nil "set the following flags: ~a"
             (format nil *english-list*
                     (setf *worker-flags*
                           (loop for (name value) in `((,must-have-tokens :must-have-tokens))
                              when name
                              collect value)))))))


(dotimes (i 16)
  (bt:make-thread #'pixelworker :name "Pixel placing worker"))

(push (create-folder-dispatcher-and-handler
       "/static/"
       (merge-pathnames "static/" (asdf:system-source-directory :pixelcanvas-bot)))
      *dispatch-table*)

(hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port 8080
                                  :read-timeout 1
                                  :write-timeout 1
                                  :output-chunking-p t))
