(in-package :pixelcanvas-bot)

(defvar db-path (merge-pathnames "proxies.sexp" (asdf:system-source-directory :pixelcanvas-bot)))

(defun read-proxies (fname)
  (with-open-file (fd fname :if-does-not-exist :error)
    (setf *proxies*
	  (loop for (proxy fingerprint) in (read fd)
	     collect (make-pixelcanvas-client :proxy proxy
					      :fingerprint fingerprint)))))

(defun write-proxies (fname)
  (with-open-file (fd fname :direction :output :if-exists :supersede)
    (print (loop for proxy in *proxies*
	      collect (list (pixelcanvas-client-proxy proxy)
			    (pixelcanvas-client-fingerprint proxy)))
	   fd)))

(defun dump-proxies ()
  (write-proxies db-path))

(bt:make-thread
 (lambda ()
   (loop
     (sleep 30)
     (dump-proxies))))

(exit-hooks:add-exit-hook #'dump-proxies)
(when (probe-file db-path)
  (read-proxies db-path))
