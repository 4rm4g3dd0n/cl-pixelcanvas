(in-package :pixelcanvas-bot)

(defvar *captcha-queue* (make-queue :name "manual captcha queue"))
(defvar *captcha-expire-time* 120)

(defun add-token (token)
  (format t "Added token ~a~%" token)
  (enqueue (cons token
                 (+ (get-internal-real-time)
                    (* *captcha-expire-time*
                       internal-time-units-per-second)))
           *captcha-queue*))

(defun get-token ()
  (loop
       (multiple-value-bind (value success)
           (dequeue *captcha-queue*)
         (unless success
           (return nil))
         (when value
           (destructuring-bind (token . expiry-time) value
             (when (> expiry-time (get-internal-real-time))
               (format t "got ~a~%" token)
               (return token)))))))

(define-easy-handler (http-add-token :uri "/add-token") (token)
  ;; meh
  (setf (header-out "Access-Control-Allow-Origin") "*")
  (if token
      (progn
        (add-token token)
        (format nil "ok thanks, added ~a." token))
      "nice try, Getulix"))

(defun reset-pixelcanvas-proxy (proxy &optional token)
  (format t "resetting ~a with ~a~%"
          (pixelcanvas-client-proxy proxy)
          token)
  (setf (pixelcanvas-client-err proxy) nil)
  (setf (pixelcanvas-client-fingerprint-ok proxy) t)
  (setf (pixelcanvas-client-captcha-token proxy) token)
  (setf (pixelcanvas-client-wait-time proxy) (get-internal-real-time)))

(defun token-apply-thread ()
  (loop
     (loop
        for proxy in *proxies*
        when (and (null (pixelcanvas-client-err proxy))
                  (not  (pixelcanvas-client-fingerprint-ok proxy)))
        do (let ((token (get-token)))
             (when token
               (reset-pixelcanvas-proxy proxy token))))
     (sleep 0.5)))

(bt:make-thread #'token-apply-thread :name "Token applying thread")
