(in-package :pixelcanvas-bot)

(defun download-png (url)
  (pixelcanvas::read-png-stream
   (flexi-streams:make-in-memory-input-stream
    (drakma:http-request url :connection-timeout 5))))

(defun rescan-image (image)
  (let* ((png (download-png
               (pixelcanvas-image-name image)))
         (new-pixels
          (filter-wrong png
                        :offset-x (pixelcanvas-image-offset-x image)
                        :offset-y (pixelcanvas-image-offset-y image))))
    (bt:with-lock-held (*buffer-lock*)
      (setf (pixelcanvas-image-pixels image)
            new-pixels))))

(defun rescan-thread (&optional (time 120))
  (loop
     (loop for image in *pixel-buffer*
        unless (member :no-rescan (pixelcanvas-image-flags image))
        do (ignore-errors
             (rescan-image image)))
     (sleep time)))

(bt:make-thread #'rescan-thread :name "Image refreshing thread")
          
