(in-package :pixelcanvas-bot)

(defvar *shared-key* "GoogleMurrayBookchin")
(defvar *english-list* ; oh god no
  "~{~#[~;~:(~a~)~;~:(~a~) and ~:(~a~)~:;~@{~a~#[~;, and ~:;, ~]~}~]~}")

(defmacro basic-page (title &body body)
  `(with-html-output-to-string (*standard-output* nil :indent t :prologue t)
     (htm
      (:html
       (:head
        (:meta :charset "utf-8")
        (:meta :name "viewport"
               :content "width=device-width, initial-scale=1")
        (:link :rel "stylesheet"
               :href "/static/mini-default.min.css")
        (:link :rel "stylesheet"
               :href "/static/cl-pixelcanvas.min.css")
        (:title ,title))
       (:body
        ,@body)))))

(defmacro navbar ()
  `(htm
    (:header
     (:span :class "logo" "cl-pixelcanvas")
     (loop for (url name) in *pages*
        do (htm (:a :class "button" :href url (esc name)))))))

(defmacro redirect-page (title url &body body)
  `(with-html-output-to-string (*standard-output* nil :indent t :prologue t)
     (htm
      (:html
       (:head
        (:meta :charset "utf-8")
        (:meta :http-equiv "refresh"
               :content ,(format nil "1; url=~a" url))
        (:link :rel "stylesheet"
               :href "/static/mini-default.min.css")
        (:title ,title))
       (:body
        ,@body
        (:hr)
        (:small
         ,(format nil "Redirecting you to ~a..." url)))))))

(defmacro slow-redirect-page (title url &body body)
  `(with-html-output-to-string (*standard-output* nil :indent t :prologue t)
     (htm
      (:html
       (:head
        (:meta :charset "utf-8")
        (:meta :http-equiv "refresh"
               :content ,(format nil "10; url=~a" url))
        (:link :rel "stylesheet"
               :href "/static/mini-default.min.css")
        (:title ,title))
       (:body
        ,@body
        (:hr)
        (:small
         ,(format nil "Redirecting you to ~a..." url)))))))

(defmacro with-authentication (&body body)
  `(if (session-value 'auth)
       (progn
         ,@body)
       (progn
         (setf (return-code*) 403)
         (basic-page "Not authorized"
           (:h1 "Access denied!")
           (:p
            "You are not logged in. Click"
            (:a :href "/login-form" "here")
            "to log in.")))))

(defmacro card ((&rest types) title &body body)
  `(htm
    (:div :class ,(format nil "card fluid ~{~(~a~)~^ ~}" types)
          (:h3 ,title)
          ,@body)))

(defmacro warning-card (title &body body)
  `(card (:warning) ,title ,@body))

(define-easy-handler (http-login-form :uri "/login-form") ()
  (basic-page "Login"
    (:form :method "POST"
           :action "/login"
           (:h1 "Log in to cl-pixelcanvas")
           (:label :for "key" "Key: ")
           (:input :name "key")
           (:button :type "submit" "Log in"))))

(define-easy-handler (http-login :uri "/login") (key)
  (if (equal key *shared-key*)
      (progn
        (setf (session-value 'auth)
              (random (expt 2 32)))
        (redirect-page "Logging in..."
                       "/"
                       "All good. Taking you to the home page..."))
      (progn
        (setf (return-code*) 403)
        "No, that's not it.")))

(define-easy-handler (http-logout :uri "/logout") ()
  (setf (session-value 'auth) nil)
  (basic-page "Logged out"
    (:h1 "Logged out")
    (:p "You have been logged out. See you again soon!")))
