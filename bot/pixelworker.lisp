(in-package :pixelcanvas-bot)

(defstruct pixelcanvas-image
  (pixels nil :type list)
  (name "" :type string)
  (offset-x 0 :type fixnum)
  (offset-y 0 :type fixnum)
  (flags nil :type list))
  
(defvar *stray-pixels* nil)
(defvar *worker-flags* nil)

(defun random-item (list)
  (nth (random (length list)) list))

(defun find-worker (list)
  (loop for worker in list
     when (and (pixelcanvas-client-fingerprint-ok worker)
	       (pixelcanvas-readyp worker)
               (null (pixelcanvas-client-err worker))
               (or (not (member :must-have-tokens *worker-flags*))
                   (pixelcanvas-client-captcha-token worker)))
     do (return worker)))

(defun get-pixel ()
  (if *stray-pixels*
      (bt:with-lock-held (*buffer-lock*)
        (pop *stray-pixels*))
      (let ((first-image (first *pixel-buffer*)))
        (when first-image
          (bt:with-lock-held (*buffer-lock*)
            (if (pixelcanvas-image-pixels first-image)
                (destructuring-bind (px py colour)
                    (pop (pixelcanvas-image-pixels first-image))          
                  (when (null (pixelcanvas-image-pixels first-image))
                    (pop *pixel-buffer*))
                  (list (+ px (pixelcanvas-image-offset-x first-image))
                        (+ py (pixelcanvas-image-offset-y first-image))
                        colour))
                (pop *pixel-buffer*)))))))

(defvar *telemetry-lock* (bt:make-lock "Telemetry lock"))
(defvar *telemetry-stack* nil)
(defun put-telemetry (&rest status)
  (bt:with-lock-held (*telemetry-lock*)
    (push status *telemetry-stack*)))

(defun put-back-pixel (pixel)
  (bt:with-lock-held (*buffer-lock*)
    (push pixel *stray-pixels*)))

(defun handle-pixel (pixel)
  (destructuring-bind (x y colour) pixel
    (let* ((next-proxy (find-worker *proxies*))
           (token (when next-proxy
                    (pixelcanvas-client-captcha-token next-proxy))))
      (if next-proxy
          (handler-case
              (multiple-value-bind (json status)
                  (progn
                    (pixelcanvas-wait next-proxy 400)
                    (send-pixel x y colour next-proxy))
                (put-telemetry x y colour next-proxy status token)
                (case status
                  (200
                   ;; All good.
                   (pixelcanvas-wait next-proxy
                                     (or (cdr (assoc :wait-seconds json))
                                         0)))
                  (400
                   ;; We plotted too early.
                   (pixelcanvas-wait next-proxy
                                     (or (cdr (assoc :wait-seconds json))
                                         0))
                   (put-back-pixel pixel))
                  (422
                   ;; Proxy broke.
                   (setf (pixelcanvas-client-fingerprint-ok next-proxy) nil)
                   (put-back-pixel pixel))
                  (otherwise
                   ;; Ummm.....
                   (format t "Odd response code ~d~%" status)
                   (setf (pixelcanvas-client-err next-proxy)
                         (format nil "Odd response code ~d, response ~s" status json))
                   (put-back-pixel pixel))))
            (condition (e)
              (format t "*** ~a~%" e)
              (put-back-pixel pixel)
              (setf (pixelcanvas-client-err next-proxy)
                    (format nil "Lisp error: ~a" e))))
          (progn
            (put-back-pixel pixel)
            (sleep 0.1))))))

(defun pixelworker ()
  (loop
     (let ((pixel (get-pixel)))
       (if pixel
           (handle-pixel pixel)
           (sleep 0.1)))))
