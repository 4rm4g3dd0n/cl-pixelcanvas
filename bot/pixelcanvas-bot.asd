(defsystem :pixelcanvas-bot
  :depends-on (:hunchentoot
               :cl-who
               :exit-hooks
               :bordeaux-threads
               :split-sequence
               :flexi-streams
               :drakma
               :pixelcanvas
               :cl-json
               :safe-queue)
  :components ((:file "package")
               (:file "pages")
               (:file "pixelworker")
               (:file "db")
               (:file "manualcaptcha")
               (:file "httpbot")))
