(defpackage :pixelcanvas
  (:use :cl :split-sequence)
  (:export :make-pixelcanvas-client
	   :floor-to-grid
	   :pixelcanvas-client-proxy
	   :pixelcanvas-client-fingerprint
	   :pixelcanvas-client-cookies
	   :pixelcanvas-client-wait-time
           :pixelcanvas-client-err
	   :pixelcanvas-client-fingerprint-ok
           :pixelcanvas-client-captcha-token
	   :myself
	   :send-pixel
	   :pixelcanvas-wait
	   :pixelcanvas-readyp
	   :*cover-agent*
	   :*headers*
	   :*base-url*
	   :filter-wrong
	   :pixel-already-placed-p))
