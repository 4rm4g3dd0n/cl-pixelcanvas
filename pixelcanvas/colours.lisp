(in-package :pixelcanvas)

(defvar *colours*
  '((0 :white 255 255 255)
    (1 :gainsboro 228 228 228)
    (2 :grey 136 136 136)
    (3 :nero 34 34 34)
    (4 :pink 255 167 209)
    (5 :red 229 0 0)
    (6 :orange 229 149 0)
    (7 :brown 160 106 66)
    (8 :yellow 229 217 0)
    (9 :conifer 148 224 68)
    (10 :green 0 190 0)
    (11 :turquoise 0 211 221)
    (12 :pacific-blue 0 131 199)
    (13 :blue 0 0 234)
    (14 :violet 207 110 228)
    (15 :purple 130 0 128)))

(defun colour-by-name (name)
  (assoc name *colours* :key #'second))

(defun colour-by-number (number)
  (assoc number *colours*))

(defun colour-number-by-rgb (r g b)
  ;; This is probably a very intensive section of the code,
  ;; likely due to a shitty unoptimized colour picker.
  (declare (type fixnum r g b)
	   (optimize (speed 3) (safety 1)))
  (let ((best-colour -1)
	(best-difference 1000))
    (loop
       for (number nil ar ag ab) in *colours*
       for this-difference = (+
			      (abs (- r ar))
			      (abs (- g ag))
			      (abs (- b ab)))
       do (cond
	    ;; If our colour is really close, it probably is that colour. Stop and return our colour.
	    ((< this-difference 16)
	     (setf best-colour number)
	     (return))
	    ;; If our colour is closer than the last best colour, make that the new best.
	    ((< this-difference best-difference)
	     (setf best-difference this-difference)
	     (setf best-colour number))
	    ;; Continue. :c
	    (t nil)))
    best-colour))
