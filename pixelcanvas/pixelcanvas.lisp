(in-package :pixelcanvas)

(defvar *cover-agent* "Mozilla/5.0 (Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36)")
(defvar *base-url* "http://pixelcanvas.io/")
(defvar *send-pixel-format*
  "{\"x\":~d,\"y\":~d,\"color\":~d,\"fingerprint\":~s,\"token\":~a,\"a\":~d}")
(defvar *headers*
  `(("accept" . "application/json")
    ("content-type" . "application/json")
    ("origin" . ,*base-url*)
    ("referer" . ,*base-url*)))

(defvar *flooring-number* 960)

(defun floor-to-grid (n)
  (* *flooring-number* (floor n *flooring-number*)))

(defun force-string (stuff)
  (if (stringp stuff)
      stuff
      (coerce (map 'vector #'code-char stuff) 'string)))

(defun get-request (url)
  (multiple-value-bind (text status)
      (drakma:http-request url :user-agent *cover-agent* :force-binary t)
    (values (cl-json:decode-json-from-string
	     (force-string text))
	    status)))

(defun post-request (url data &optional proxy)
  (multiple-value-bind (text status)
      (drakma:http-request url :user-agent *cover-agent*
			   :accept "application/json"
			   :content-type "application/json"
			   :method :post :content data
			   :user-agent *cover-agent*
			   :additional-headers *headers*
			   :proxy proxy
			   :connection-timeout 5)
    (values (cl-json:decode-json-from-string
	     (force-string text))
	    status)))

(declaim (inline make-url))
(defun make-url (format &rest data)
  (concatenate 'string
	       *base-url*
	       (apply #'format (list* nil format data))))

(defstruct pixelcanvas-client
  (proxy nil :type list)
  (captcha-token nil :type (or string null))
  (fingerprint "" :type string)
  (cookies (make-instance 'drakma:cookie-jar) :type drakma:cookie-jar)
  (wait-time (get-internal-real-time) :type real)
  (fingerprint-ok t :type boolean)
  (err nil :type (or string null))
  (flags nil :type list))

(defun myself (client)
  (post-request (make-url "api/me")
		(format nil "{\"fingerprint\":~s}"
			(pixelcanvas-client-fingerprint client))
		(pixelcanvas-client-proxy client)))

(defun send-pixel (x y colour client)
  (let ((payload (format nil *send-pixel-format*
			 x y
			 colour
			 (pixelcanvas-client-fingerprint client)
                         (if (pixelcanvas-client-captcha-token client)
                             (format nil "~s" (pixelcanvas-client-captcha-token client))
                             "null")
			 (+ x y 8))))
    (setf (pixelcanvas-client-captcha-token client) nil)
    (post-request (make-url "api/pixel") payload
		  (pixelcanvas-client-proxy client))))

(defun pixelcanvas-wait (client time)
  (setf (pixelcanvas-client-wait-time client)
	(+ (get-internal-real-time)
	   (* internal-time-units-per-second time))))

(defun pixelcanvas-readyp (client)
  (< (pixelcanvas-client-wait-time client)
     (get-internal-real-time)))

(defun get-block (bx by)
  (declare (optimize (debug 3)))
  (let ((canvas (drakma:http-request (make-url "api/bigchunk/~d.~d.bmp" bx by)))
	(image (make-array '(960 960) :element-type '(unsigned-byte 4)))
        (nibbles (make-array '(921600) :element-type '(unsigned-byte 4))))
    (loop
       for byte across canvas
       for n from 0
       do (setf (aref nibbles (* n 2))
		(ash byte -4))
       do (setf (aref nibbles (1+ (* n 2)))
		(logand byte 15)))
    (loop
       for x from 0 to 959
       for bigx = (floor x 64)
       for smallx = (mod x 64)
       do (loop
	     for y from 0 to 959
	     for bigy = (floor y 64)
	     for smally = (mod y 64)
	     for offset = (+ smallx
			     (* smally 64)
			     (* bigx 4096)
			     (* bigy 61440))
             for nibble = (aref nibbles offset)
	     do (setf (aref image x y) nibble)))
    image))
