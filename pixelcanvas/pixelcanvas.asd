(defsystem pixelcanvas
  :depends-on (:drakma :cl-json :split-sequence :array-operations :png-read)
  :author "theemacsshibe"
  :license "Cooperative Software License v1+"
  :components ((:file "package")
	       (:file "colours" :depends-on ("package"))
	       (:file "pixelcanvas" :depends-on ("package"))
	       (:file "art" :depends-on ("colours" "package" "pixelcanvas"))))
