# cl-pixelcanvas, the newb's guide to

- If you're on Windows or a Mac, download Portacle from https://portacle.github.io/. This will save you a lot of time downloading all the software required to have a happy Lisp system.
- If you're on Linux, you can also download Portacle from the same place, or go all the way and install the important bits: SBCL, Quicklisp and Emacs. That's up to you and you can spend all week on it if you like.
- Download everything here. GitLab has a "Download as .zip" button which will help.
- If you're using Portacle, copy both folders into the folder called `projects`. The folder should look like this:

```
WhereYouPutPortacle/projects:
--  bot
--  pixelcanvas
```

- If you're using bare bits of Lisp, copy both folders into `~/quicklisp/local-projects`, with the same convention.

- Open up Portacle and click into the bottom half of the window that says `; SLIME 2.20`. You'll see the cursor
light up to show you're focused in that frame. Type the following:

```
(ql:quickload :pixelcanvas-bot)
```

- Portacle will try to help by adding in the closing parentheses, but you should add them anyway (or keep pressing the right arrow) to let it know you're done typing.
- Open up a web browser (such as this one) and navigate to http://localhost:8080/, or just click on the automagically made link.


## proxies

- You'll probably want to add some proxies, so click on "ADD PROXIES".
- Fill in the proxy IP, port, and fingerprint. If the fingerprint somehow changes or needs to be revalidated, you can safely type in the same details again and the server will just replace the old information.

## images

- I'm too lazy to work out file uploads, so hop on an image hosting service like http://coinsh.red/ or http://imgur.com/.
- Upload your template. This should have a transparent background if there's supposed to be blank areas.
- Copy the URL. Make sure that's the URL of the actual image on Imgur, cause it's quite cheeky about those nowadays. It'll probably end in `.png` if it's the image.
- Paste the URL into the appropriately labelled textbox and enter your top-left X and Y coordinates.

## image reloading and flags

You can reload images to update the bot's perception of the canvas periodically.
This isn't on by default, so you should `(load "reloadimages.lisp")` to set it
up.
This is done every two minutes to images which do not have `No-Rescan` enabled.
(`No-Rescan` can be enabled when you add an image, by pushing `:no-rescan` to
 the image's `flags` list or when adding it through the fancy HTML form.)

## licensing crap

        cl-pixelcanvas: a Common Lisp API for PixelCanvas.io and multithreaded bot server
        Copyright (C) 2018 Dale O'Really (theemacsshibe)

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.